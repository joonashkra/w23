import React from 'react'

function Reporter(props) {
  return (
    <div><p>{props.name}: {props.children}</p></div>
  )
}

export default Reporter